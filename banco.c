#include <stdio.h>
#include <stdlib.h>

#define HELIO 5
#define GRAVIDADE 10
#define GAS 50

int main(int argc, char **argv){
	int valor = atoi(argv[1]);
	/* Comentário útil */
	printf("%d gas(es)\n", valor/GAS);
	valor %= GAS;
	printf("%d gravidade(s)\n", valor/GRAVIDADE);
	valor %= GRAVIDADE;
	printf("%d helio(s)\n", valor/HELIO);
	valor %= HELIO;
	printf("%d hudrogenio(s)\n", valor);

	return 0;


}

